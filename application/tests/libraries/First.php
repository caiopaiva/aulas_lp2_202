<?php

    class First extends TestCase {
        
        public function setUp(): void { 
            $this->resetInstance();
        }


        function xtestContaDeveInserirRegistroCorretamente(){
            // cenário
            $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
            $this->CI->builder->clear();
            $this->CI->builder->start();

            // ação
            $this->CI->load->library('conta');
            $res = $this->CI->conta->lista('pagar', 4, 2021);
            
            
            //Verificação
            $this->assertEquals('Magalu', $res[0]['parceiro']); 
            $this->assertEquals(2021,$res[0]['ano']); 
            $this->assertEquals(4, $res[0]['mes']);
            $this->assertEquals(3, sizeof($res));
            
            $this->assertEquals('Casas Bahia', $res[1]['parceiro']); 
            $this->assertEquals(2021, $res[1]['ano']); 
            $this->assertEquals(4, $res[1]['mes']);
    
            $this->assertEquals('EDP Energia', $res[2]['parceiro']); 
            $this->assertEquals(100.00, $res[2]['valor']);        
            $this->assertEquals(2021, $res[2]['ano']); 
            $this->assertEquals(4, $res[2]['mes']);
        }

        function xtestContaDeveInformarTotalDeContasAPagarEAReceber() {
            $this->CI->load->library('builder/ContaDataBuilder',null, 'builder');
            $this->CI->builder->clear();
            $this->CI->builder->start();

            // ação
            $this->CI->load->library('conta');
            $res = $this->CI->conta->total('pagar', 4, 2021);

            // verificação
            $this->assertEquals(5100.00, $res); 

        }

        function testContaDeveCalcularSaldoMensal() {
            // cenario
            $this->CI->load->library('builder/ContaDataBuilder',null, 'builder');
            $this->CI->builder->clear();
            $this->CI->builder->start();

            // ação
            $this->CI->load->library('conta');
            $res = $this->CI->conta->saldo(4, 2021);

            // verificação
            $this->assertEquals(870.77, $res);
        }
    }


