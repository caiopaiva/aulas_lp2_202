<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>LPII - 2021/1</title>
    <link rel="icon" href="<?= base_url() ?>assets/mdb/img/mdb-favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">  
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/mdb.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/style.css">
    <script type="text/javascript" src="<?= base_url() ?>assets/mdb/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/util.js"></script>
  </head>
<body>

