<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-light warning-color lighten-5">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="<?= base_url('home') ?>">Controle Financeiro</a>
  
  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">
    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('home') ?>">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <!-- 1º Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Cadastro</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a>
          <a class="dropdown-item" href="#">Conta Bancária</a>
          <a class="dropdown-item" href="#">Parceiros</a>
        </div>
      </li>
      <!-- 2º Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Lançamentos</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="<?= base_url('contas/pagar') ?>">Contas a Pagar</a>
        <a class="dropdown-item" href="<?= base_url('contas/receber') ?>">Contas a Receber</a>
        <a class="dropdown-item" href="#">Fluxo de Caixa</a>
        </div>
      </li>

      <!-- 3º Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false"> Relatórios</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="#">Lançamento por Período</a>
        <a class="dropdown-item" href="<?= base_url('contas/movimento') ?>">Movimento de Caixa</a>
        <a class="dropdown-item" href="#">Resumo Anual</a>
        </div>
      </li>
    </ul>
    <!-- Links -->

  <!-- Collapsible content -->
</nav>
<!--/.Navbar-->