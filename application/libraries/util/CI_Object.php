<?php

class CI_Object{
    /**
     * 
     * Permitir que libraries acessen propriedades e nétodos do CI.
     * 
     * @param string $key
     */
    public function __get($key){
        return get_instance()->$key;
    }
}