<?php

    defined('BASEPATH') OR exit('No direct script access allowed');
    include_once APPPATH . 'libraries/util/CI_Object.php';

    class ContaDataBuilder extends CI_Object {
        
        private $contas = [
            [
                'parceiro' => 'Magalu',
                'descricao' => 'Notebook',
                'valor' => '2000',
                'mes' => 4,
                'ano' => 2021,
                'tipo' => 'pagar'
            ],
            [
                'parceiro' => 'Casas Bahia',
                'descricao' => 'Notebook',
                'valor' => '3000',
                'mes' => 4,
                'ano' => 2021,
                'tipo' => 'pagar'
            ],
            [
                'parceiro' => 'EDP Energia',
                'descricao' => 'Energia Elétrica',
                'valor' => '100.00',
                'mes' => 4,
                'ano' => 2021,
                'tipo' => 'pagar'
            ],
            [
                'parceiro' => 'Salário',
                'descricao' => '',
                'valor' => '3549.23',
                'mes' => 4,
                'ano' => 2021,
                'tipo' => 'receber'
            ],
            [
                'parceiro' => 'Aluguel',
                'descricao' => 'Casa Alugada',
                'valor' => '680',
                'mes' => 4,
                'ano' => 2021,
                'tipo' => 'receber'
            ],
            [
                'parceiro' => 'EDP Energia',
                'descricao' => 'Energia Elétrica',
                'valor' => '90.00',
                'mes' => 5,
                'ano' => 2021,
                'tipo' => 'pagar'
            ]       
        ];

        public function start(){
            $this->load->library('conta');

            foreach($this->contas as $conta){    
              $this->conta->cria($conta);
            }
        }

        public function clear() {
            $this->db->truncate('conta');
        }


    }